/*
Filename: kuts_lab_19_1
Студент: Куць Ганна Олександрівна
Группа: КН-1-3
Дата створення: 05.03.22
Лабораторна робота №19
Варіант 4
*/
#include <assert.h>
#include <stdio.h>
#include <iostream>
#include <windows.h>
using namespace std;

//вузол дерева
struct node
{
	int x;
	node *l;
	node *r;
};

// виведення на екран елементів дерева з його нащадками
void show_childr(node *&mt)
{
	node *cl, *cr;
	if (mt != NULL) //перевірка на порожній вузол
	{
		cout << " Вузол " << " ";  //виведення значення в поточному вузлі
		cout << " Значення вузла = " << mt->x << " "; //виведення значення в поточному вузлі
		
		if ((mt->l)!=NULL) //перевірка на порожній вузол
		{
			cl= mt->l;
			cout << " Лівий нащадок = " << cl->x << " ";
		}
		if ((mt->r)!=NULL)
		{
			cr= mt->r;
			cout << " Правий нащадок = " << cr->x << " ";
		}
		cout << ";\n";
		
		show_childr(mt->l); //виклик рекурсивної функції для переходу до лівого вузла
		show_childr(mt->r); //виклик рекурсивної функції для переходу до правого вузла
	}
}

// рекурсивна функція для підрахунку вузлів дерева 
int count(node *&mt)
{
	if (mt == NULL) //перевірка на порожній вузол
	return 0;
	return 1+count(mt->l)+count(mt->r);
}

// очищення дерева
void del(node *&mt)
{
	if (mt != NULL) //перевірка на порожній вузол
	{
		del(mt->l); //виклик рекурсивної функції для переходу до лівого вузла
		del(mt->r); //виклик рекурсивної функції для переходу до правого вузла
		delete mt; //видаляємо вузол
	}
	mt=NULL;
}

// додаємо вузол у збалансоване дерево
void add_node_balans(int x, node *&mt)
{
	if (NULL == mt)
	{
		mt = new node;
		mt->x = x;
		mt->l = NULL;
		mt->r = NULL;
	}
	
	else
	{
		if (count(mt->l)<count(mt->r))
		{
			if (mt->l != NULL)
			add_node_balans(x, mt->l); //виклик рекурсивної функції для переходу до лівого вузла
			else
			{
				mt->l = new node;
				mt->l->l = NULL;
				mt->l->r = NULL;
				mt->l->x = x;
			}
		}
		else
		{
			if (mt->r != NULL)
			add_node_balans(x, mt->r); //виклик рекурсивної функції для переходу до правого вузла

			else
			{
				mt->r = new node;
				mt->r->l = NULL;
				mt->r->r = NULL;
				mt->r->x = x;
			}
		}
	}
}

bool find_node(node *&mt, int e)
{
	int q=0;
	if (mt != NULL)
	{
		q=find_node(mt->l, e);
		if (mt->x == e)
		return 1;
		q=q+find_node(mt->r, e);
		return q;
	}	
	else
	return 0;
}

void r_node(node *& mt, int k, bool r)
{
	node *cr, *cl;
	if(mt != NULL) //перевірка на порожній вузол
	{
		if(mt->x == k)
		{
			r = false;
		}
		if(r)
		{
			if ((mt->l) != NULL) 
		    {
			    cl = mt->l;
            }
            if ((mt->r) != NULL) 
			{
                cl = mt->r;
            }
		}
		else
		{
			cout<<" Вузол : "<<" ";
			cout<<" Значення вузла = "<<mt->x<<" ";
			if((mt->l) != NULL)
			{
				cl = mt->l;
				cout<<" Лівий нащадок : "<<cl->x<<" ";
			}
			if((mt->r) != NULL)
			{
				cr = mt->r;
				cout<<" Правий нащадок : "<<cr->x<<" ";
			}
			else return;
			printf("\n");
		}
		r_node(mt->l, k, r);
		r_node(mt->r, k, r);
	}
}

int main()
{
	const int n = 15;
	int i, e;
	int el[n]={1,12,15,4,6,8,23,9,17,28,24,18,3,7,21};
	
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	
	node *tree = NULL;
	cout << "\nБудуємо збалансоване дерево "<<"\n";
	i=0;
	
	while (i<n)
	{
		add_node_balans(el[i], tree);
		i++;
	}
	
	cout << "\nКількість вузлів дерева: " <<count(tree)<<"\n";
	cout << "\nЗгенеровані вузли дерева: \n";
	show_childr(tree);
	
	cout << "\nВведіть елемент, який необхідно знайти: ";
	cin >> e;
	
	if(find_node(tree, e))
	cout << endl << "Такий елемент є у дереві!" << endl;
	else
	cout << endl << "Такого елементу немає у дереві!" << endl;
	
	cout<< "\nВведіть елемент для виведення: ";
	cin>>e;
	cout << endl;
	
	r_node(tree, e, true);
	printf("\n");
	
	del(tree); //очистка пам'яті виділеної під дерево
	return 0;
}

